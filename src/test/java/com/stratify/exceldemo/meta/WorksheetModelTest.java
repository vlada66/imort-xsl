package com.stratify.exceldemo.meta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WorksheetModelTest {

    WorksheetModel model;

    @BeforeEach
    void init() {
         model = new WorksheetModel();
    }

    @Test
    void initColumnIndices() {
    }

    @Test
    void extractDigits_A10() {
        Integer columnIndex = model.extractDigits("A10");
        Assertions.assertEquals(10, columnIndex);
    }

    @Test
    void extractDigits_ZZ99() {
        Integer columnIndex = model.extractDigits("ZZ99");
        Assertions.assertEquals(99, columnIndex);
    }
}
