DROP TABLE IF EXISTS booking;

CREATE TABLE booking (
                              id INT AUTO_INCREMENT  PRIMARY KEY,
                              customer_name VARCHAR(250) NOT NULL,
                              booking_date DATE NOT NULL,
                              opportunity_id VARCHAR(250) NOT NULL,
                              team VARCHAR(250),
                              product VARCHAR(250)

);
--
--INSERT INTO booking (customer_name, opportunity_id, team, product) VALUES
--('Ian', 'bde51b70-adef-4137-8805-ea15f8ca6c77', 'NORTH', 'GOLD'),
--('Ralph', '2facae1b-7a79-4db4-8b89-a4173248af57', 'NORTH', 'ALLOY');
