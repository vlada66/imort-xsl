package com.stratify.exceldemo.controller;

import com.stratify.exceldemo.converter.DateConverter;
import com.stratify.exceldemo.entity.Booking;
import com.stratify.exceldemo.meta.WorksheetModel;
import com.stratify.exceldemo.service.BookingService;
import com.stratify.exceldemo.validator.InputParamsValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/import")
@Slf4j
public class ImportController {

    @Autowired
    BookingService bookingService;

    @GetMapping("/version")
    public String version() {
        /* for testing purposes only */
        return "1.0.0";
    }

    @GetMapping("/opportunity")
    public Page<Booking> fetchBookings(
            @RequestParam(name="customerName", required = false) String customerName,
            @RequestParam(name="team", required = false) String team,
            @RequestParam(name="product", required = false) String product,
            @RequestParam(name="startDate", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name="endDate", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            Pageable pageable
    ) {
        InputParamsValidator.validateDateRange(startDate, endDate);
        return bookingService.findBookings(customerName, team, product, startDate, endDate, pageable);
    }


    @PostMapping("/upload")
    public Integer importExcel(@RequestParam("file") MultipartFile xlsFile, @RequestParam String worksheetName, @RequestParam String boundary) throws IOException {
        List<Booking> bookings = new ArrayList<>();
        Set<String> importedOpportunities = new HashSet<>();
        XSSFWorkbook workbook = new XSSFWorkbook(xlsFile.getInputStream());
        Map<WorksheetModel.COLUMN, Integer> columnsIndices = getColumnsIndices(boundary);

        XSSFSheet worksheet = pickRequestedSheet(workbook, worksheetName);
        int firstRowToParse = 3; //TODO get from WorksheetModel
        int customerNameColumnIndex = columnsIndices.get(WorksheetModel.COLUMN.CUSTOMER_NAME);
        int bookingDateColumnIndex = columnsIndices.get(WorksheetModel.COLUMN.BOOKING_DATE);
        int opportunityColumnIndex = columnsIndices.get(WorksheetModel.COLUMN.OPPORTUNITY_ID);
        int teamColumnIndex = columnsIndices.get(WorksheetModel.COLUMN.TEAM);
        int productColumnIndex = columnsIndices.get(WorksheetModel.COLUMN.PRODUCT);

        for (int index = firstRowToParse; index < worksheet.getPhysicalNumberOfRows(); index++) {
            XSSFRow row = worksheet.getRow(index);
            if (!imported(row, opportunityColumnIndex, importedOpportunities)) {

                Booking booking = new Booking();
                booking.setCustomerName(row.getCell(customerNameColumnIndex).getStringCellValue());
                booking.setBookingDate(DateConverter.toLocalDate(row.getCell(bookingDateColumnIndex).getDateCellValue()));

                String opportunityId = row.getCell(opportunityColumnIndex).getStringCellValue();
                importedOpportunities.add(opportunityId);
                booking.setOpportunityId(opportunityId);

                booking.setTeam(row.getCell(teamColumnIndex).getStringCellValue());
                booking.setProduct(row.getCell(productColumnIndex).getStringCellValue());
                bookings.add(booking);
            }
        }
        bookingService.save(bookings);
        return bookings.size();
    }

    private Map<WorksheetModel.COLUMN, Integer> getColumnsIndices(String boundary) {
        WorksheetModel worksheetModel = new WorksheetModel();
        worksheetModel.initColumnIndices(boundary);
        Map<WorksheetModel.COLUMN, Integer> columnsIndices = worksheetModel.getColumns();
        return columnsIndices;
    }

    private boolean imported(XSSFRow row, int keyColumnIndex, Set<String> alreadyImportedValues) {
        String cellValue = row.getCell(keyColumnIndex).getStringCellValue();
        return alreadyImportedValues.contains(cellValue);
    }

    private XSSFSheet pickRequestedSheet(XSSFWorkbook workbook, String sheetName) {
        int allSheetsSize = workbook.getNumberOfSheets();
        for (int i = 0; i < allSheetsSize; i++) {
            XSSFSheet sheet = workbook.getSheetAt(i);
            if (sheet != null && sheet.getSheetName().equals(sheetName)) {
                return sheet;
            }
        }
        throw new IllegalArgumentException("Unable to find sheetName " + sheetName);
    }
}
