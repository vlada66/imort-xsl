package com.stratify.exceldemo.meta;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorksheetModel {
    int firstRowToImport;

    public enum COLUMN {
        CUSTOMER_NAME ("CustomerName"),
        BOOKING_DATE("BookingDate"),
        OPPORTUNITY_ID("OpportunityID"),
        BOOKING_TYPE("BookingType"),
        TOTAL("Total"),
        ACCOUNT_EXECUTIVE("AccountExecutive"),
        SALES_ORGANIZATION("SalesOrganization"),
        TEAM("Team"),
        PRODUCT("Product"),
        RENEWABLE("Renewable");

        public final String code;

        private COLUMN(String code) {
            this.code = code;
        }
    }

    private final Map<COLUMN, Integer> columnsMap = new HashMap<>();

    public Map<COLUMN, Integer> getColumns() {
        return columnsMap;
    }

    public int getFirstRowToImport() {
        return firstRowToImport;
    }

    /**
    * <p>Method should initialize columnsMap to have correct indices for worksheet upload feature. </p>
    *  @param boundary in in the form of "A1:D20" left-upper-corder:right-bottom-corner
    */
    public void initColumnIndices(String boundary) {
        String[] boundaryParts = boundary.split(":");
        //TODO this value should be used as offset
        Integer leftUpperCornerIndex = extractDigits(boundaryParts[0]);
        //TODO this value should be used to check if we need to parse a column when uploading xsl
        Integer rightBottomCornerIndex = extractDigits(boundaryParts[1]);

        columnsMap.put(COLUMN.CUSTOMER_NAME, 1);
        columnsMap.put(COLUMN.BOOKING_DATE, 2);
        columnsMap.put(COLUMN.OPPORTUNITY_ID, 3);
        columnsMap.put(COLUMN.BOOKING_TYPE, 4);
        columnsMap.put(COLUMN.ACCOUNT_EXECUTIVE, 6);
        columnsMap.put(COLUMN.SALES_ORGANIZATION, 7);
        columnsMap.put(COLUMN.TEAM, 8);
        columnsMap.put(COLUMN.PRODUCT, 9);
        columnsMap.put(COLUMN.RENEWABLE, 10);

        firstRowToImport = resolveFirstRowToImport();

    }

    /**
     * <p>We can iterate over rows to find headers (maybe there is in POI already such a method???
     *  Then we know, that we should start upload . </p>
     *
     */
    private int resolveFirstRowToImport() {
        return 3;
    }

    protected Integer extractDigits(String letterAndDigits) {
        String columnIndexStr = "1";
        String digitsRegex = "\\d+";
        Pattern digitPattern = Pattern.compile(digitsRegex);
        Matcher matcher = digitPattern.matcher(letterAndDigits);
        if(matcher.find()) {
            columnIndexStr = matcher.group();
        }
        return Integer.valueOf(columnIndexStr);
    }


}
