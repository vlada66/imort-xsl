package com.stratify.exceldemo.validator;

import java.time.LocalDate;

public class InputParamsValidator {

    public static void validateDateRange(LocalDate startDate, LocalDate endDate) {
        if( startDate != null && endDate != null && endDate.isBefore(startDate)) {
            throw new IllegalArgumentException("Error: startDate must be before endDate. ");
        }
    }
}
