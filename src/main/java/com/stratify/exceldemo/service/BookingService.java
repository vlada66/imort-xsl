package com.stratify.exceldemo.service;

import com.stratify.exceldemo.entity.Booking;
import com.stratify.exceldemo.entity.Booking_;
import com.stratify.exceldemo.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class BookingService {

    final
    BookingRepository bookingRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public void save(List<Booking> bookings) {
        bookingRepository.saveAll(bookings);
    }

    public Page<Booking> findBookings(String customerName, String team, String product, LocalDate startDate, LocalDate endDate, Pageable pageable) {
        Specification<Booking> spec = Specification
                .where(BookingSpecs.getBookingsByStringSpec(customerName, Booking_.customerName))
                .and(BookingSpecs.getBookingsByStringSpec(team, Booking_.team))
                .and(BookingSpecs.getBookingsByStringSpec(product, Booking_.product))
                .and(BookingSpecs.getBookingsAfterDateSpec(startDate, Booking_.bookingDate))
                .and(BookingSpecs.getBookingsBeforeDateSpec(endDate, Booking_.bookingDate));
        return bookingRepository.findAll(spec, pageable);
    }
}
