package com.stratify.exceldemo.service;

import com.stratify.exceldemo.entity.Booking;
import com.stratify.exceldemo.entity.Booking_;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.time.LocalDate;

public class BookingSpecs {


    public static Specification<Booking> getBookingsByStringSpec(String requestParam, SingularAttribute<Booking, String> metaAttribute) {
        return (root, query, criteriaBuilder) -> {
            if(StringUtils.isBlank(requestParam))
                return criteriaBuilder.conjunction();
            else {
                return  criteriaBuilder.equal(root.get(metaAttribute), requestParam);
            }
        };
    }

    public static Specification<Booking> getBookingsAfterDateSpec(LocalDate requestParam, SingularAttribute<Booking, LocalDate> metaAttribute) {
        return (root, query, criteriaBuilder) -> {
            if(requestParam == null)
                return criteriaBuilder.conjunction();
            else {
                return  criteriaBuilder.greaterThanOrEqualTo(root.get(metaAttribute), requestParam);
            }
        };
    }

    public static Specification<Booking> getBookingsBeforeDateSpec(LocalDate requestParam, SingularAttribute<Booking, LocalDate> metaAttribute) {
        return (root, query, criteriaBuilder) -> {
            if(requestParam == null)
                return criteriaBuilder.conjunction();
            else {
                return  criteriaBuilder.lessThanOrEqualTo(root.get(metaAttribute), requestParam);
            }
        };
    }
}
